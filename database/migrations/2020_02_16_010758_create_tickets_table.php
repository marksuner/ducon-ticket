<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id'); // creator
            $table->integer('admin_id')->nullable(); // admin who works on this
            $table->integer('supporter_id')->nullable(); // assigned user
            $table->integer('item_id')->nullable(); // requesting item
            $table->enum('status', ['pending', 'assigned', 'done']); // can add 'in progress'
            $table->string('title');
            $table->string('comment', 250);
            $table->dateTime('seen_at')->nullable();
            $table->timestamps();

            // $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            // $table->foreign('admin_id')->references('id')->on('users')->onDelete('cascade');
            // $table->foreign('supporter_id')->references('id')->on('users')->onDelete('cascade');
            // $table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
