<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Item;
use Faker\Generator as Faker;

$factory->define(Item::class, function (Faker $faker) {
    $type = $faker->randomDigit % 2 === 0 ? 'hardware' : 'software';
    
    return [
        'name' => $faker->word,
        'description' => $faker->sentence,
        'type' => $type,
    ];
});
