<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Ticket;
use Faker\Generator as Faker;

$factory->define(Ticket::class, function (Faker $faker) {
    $user = User::all()->random()->first();

    return [
        'title' => $faker->sentence,
        'comment' => $faker->sentence,
        'user_id' => $user->id,
        'admin_id' => $user->id,
        'supporter_id' => $user->id,
        'item_id' => null,
        'status' => 'pending',
        'seen_at' => null,
    ];
});
