<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Notification;
use App\User;
use Faker\Generator as Faker;

$factory->define(Notification::class, function (Faker $faker) {
    $user = User::all()->random()->first();

    return [
        'title' => $faker->sentence,
        'content' => $faker->paragraph,
        'user_id' => $user->id,
    ];
});
