<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Trail;
use App\User;
use Faker\Generator as Faker;

$factory->define(Trail::class, function (Faker $faker) {
    $user = User::all()->random()->first();

    return [
        'user_id' => $user->id,
        'description' => $faker->sentence,
        'old_aux' => json_encode([
            'title' => 'old title',
        ]),
        'new_aux' => json_encode([
            'title' => 'new title',
        ])
    ];
});
