<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = Role::where('name', 'admin')->first();
        $memberRole = Role::where('name', 'member')->first();
        $supportRole = Role::where('name', 'support')->first();

        $users = [
            [
                'user' => [
                    'name' => 'Admin',
                    'email' => 'admin@admin.com',
                    'password' => bcrypt('admin'),
                    'role_id' => $adminRole->id,
                ],
                'person' => [
                    'last_name' => 'Admin',
                    'first_name' => 'Admin',
                    'mobile_no' => '09156402030'
                ]
            ],
            [
                'user' => [
                    'name' => 'Support',
                    'email' => 'support@support.com',
                    'password' => bcrypt('support'),
                    'role_id' => $supportRole->id,
                ],
                'person' => [
                    'last_name' => 'Support',
                    'first_name' => 'Support',
                    'mobile_no' => '09156402030'
                ]
            ],
            [
                'user' => [
                    'name' => 'Member',
                    'email' => 'member@member.com',
                    'password' => bcrypt('member'),
                    'role_id' => $memberRole->id,
                ],
                'person' => [
                    'last_name' => 'Member',
                    'first_name' => 'Member',
                    'mobile_no' => '09156402030'
                ]
            ]
        ];

        foreach($users as $user) {
            $model = User::create($user['user']);
            $model->person()->create($user['person']);
        }
    }
}
