<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name' => 'admin',
            ],
            [
                'name' => 'support'
            ],
            [
                'name' => 'member'
            ]
        ];

        foreach($roles as $role) {
            Role::create($role);
        }
    }
}
