<div class="app-sidemenu">
    <div class="app-sidemenu-item app-sidemenu-avatar">
        <img src="https://via.placeholder.com/150" alt="Avatar" class="avatar">
        <h5 class="avatar--name"> {{ auth()->user()->name }} </h5>
    </div>

    <div class="app-sidemenu-item">
        <div class="row">
            <div class="col-3">
                <strong>Email</strong>
            </div>
            <div class="col-9">
                {{ auth()->user()->email }}
            </div>
        </div>
    </div>
    
    <div class="app-sidemenu-item">
        <div class="row">
            <div class="col-3">
                <strong>Mobile</strong>
            </div>
            <div class="col-9">
                {{ auth()->user()->person->mobile_no }}
            </div>
        </div>
    </div>

    <div class="app-sidemenu-item">
        <div class="row">
            <div class="col-3">
                <strong>Role</strong>
            </div>
            <div class="col-9">
                {{ auth()->user()->role->name }}
            </div>
        </div>
    </div>

    <div class="app-sidemenu-link">
        <a href="#" class="app-sidemenu-link--item {{ route_active('home') }}">
            Dashboard
        </a>
        <a href="#" class="app-sidemenu-link--item {{ route_active('tickets') }}">
            <i class="fas fa-book"></i> Tickets
        </a>
        <a href="#" class="app-sidemenu-link--item {{ route_active('users/info') }}">
            <i class="fas fa-user"></i> User Info
        </a>
        <a href="#" class="app-sidemenu-link--item {{ route_active('announcements') }}">
            <i class="fas fa-pencil-alt"></i> Announcements
        </a>
    </div>
</div>