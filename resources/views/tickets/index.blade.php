@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Tickets</div>

                <div class="card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th colspan="15">
                                    <form action="/tickets" method="get">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group mb-2 ml-2">
                                                    <label for="title" class="sr-only">Title</label>
                                                    <input type="text" class="form-control" id="title" name="title" placeholder="Title">
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group mb-2 ml-2">
                                                    <label for="comment" class="sr-only">Comment</label>
                                                    <input type="text" class="form-control" id="comment" name="comment" placeholder="Comment">
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group mb-2 ml-2">
                                                    <label for="user_id" class="sr-only">Created by:</label>
                                                    <select name="user_id" id="user_id" class="form-control">
                                                        <option value="">Created by</option>
                                                        @foreach($adminUser as $aUser)
                                                            <option value="{{$aUser->id}}">
                                                                {{ $aUser->name }}
                                                            </option>
                                                        @endforeach
                                                        @foreach($memberUser as $mUser)
                                                            <option value="{{$mUser->id}}">
                                                                {{ $mUser->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group mb-2 ml-2">
                                                    <label for="comment" class="sr-only">Support by:</label>
                                                    <select name="supporter_id" id="supporter_id" class="form-control">
                                                        <option value="">Support by</option>
                                                        @foreach($adminUser as $aUser)
                                                            <option value="{{$aUser->id}}">
                                                                {{ $aUser->name }}
                                                            </option>
                                                        @endforeach
                                                        @foreach($supportUser as $sUser)
                                                            <option value="{{$sUser->id}}">
                                                                {{ $sUser->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group mb-2 ml-2">
                                                    <label for="comment" class="sr-only">Status:</label>
                                                    <select name="status" id="status" class="form-control">
                                                        <option value="">Status</option>
                                                        <option value="pending">Pending</option>
                                                        <option value="assigned">Assigned</option>
                                                        <option value="done">Done</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <button type="submit" class="btn btn-primary mb-2 ml-2">
                                                    Search
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Title</th>
                                <th>Comment</th>
                                <th>Created by</th>
                                <th>Supported by</th>
                                <th>Item</th>
                                <th width="100px">
                                    <a href="/tickets/create" class="btn btn-sm btn-primary">
                                        Create
                                    </a>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($tickets as $ticket)
                            <tr>
                                <td class="text-center">
                                    @php
                                        $btn = 'warning';

                                        switch($ticket->status) {
                                            case 'pending':
                                                $btn = 'warning';
                                                break;
                                            case 'assigned':
                                                $btn = 'secondary';
                                                break;
                                            case 'done':
                                                $btn = 'success';
                                                break;
                                        }
                                    @endphp
                                    <span class="btn-sm btn-{{$btn}} mt-2">
                                        {{ $ticket->status }}
                                    </span>
                                    <br />
                                    <a href="/tickets/{{$ticket->id}}">{{ id_pad($ticket->id) }} </a>
                                </td>
                                <td> {{ app_str_limit($ticket->title, 25) }}</td>
                                <td> {{ app_str_limit($ticket->comment) }}</td>
                                <td> {{ $ticket->createdBy ? $ticket->createdBy->name : null }}</td>
                                <td> {{ $ticket->supportedBy ? $ticket->supportedBy->name : null }}</td>
                                <td> {{ $ticket->item ? $ticket->item->name : null }}</td>
                                <td>
                                    <a href="/tickets/{{$ticket->id}}/edit" class="btn-secondary btn btn-sm">
                                        <i class="far fa-edit"></i>
                                    </a>
                                    <a href="#" class="btn btn-sm btn-danger delete" data-url="/tickets/{{$ticket->id}}">
                                        <i class="fas fa-trash"></i>
                                    </a>
                                </td>
                            </tr>

                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="10" class="align-content-center">
                                    {{ $tickets->links() }}
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
