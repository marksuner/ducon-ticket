@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Form</div>

                <div class="card-body">
                    <form action="/tickets" method="post">
                        
                        @csrf

                        <div class="form-group row">
                            <label for="ticket" class="col-md-4 col-form-label text-md-right">
                                Title
                            </label>

                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" required autocomplete="title" autofocus>

                                @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="comment" class="col-md-4 col-form-label text-md-right">
                                Comment
                            </label>

                            <div class="col-md-6">
                                <textarea name="comment" id="comment" class="form-control @error('comment') is-invalid @enderror" required>{{ old('comment') }}</textarea>

                                @error('comment')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="status" class="col-md-4 col-form-label text-md-right">
                                Status
                            </label>

                            <div class="col-md-6">
                                <select name="status" id="status" class="form-control">
                                    <option selected value="pending">Pending</option>
                                    <option value="assigned">Assigned</option>
                                    <option value="done">Done</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="support_id" class="col-md-4 col-form-label text-md-right">
                                Support by
                            </label>

                            <div class="col-md-6">
                                <select name="support_id" id="support_id" class="form-control">
                                    @foreach($supportUser as $sUser)
                                        <option value="{{$sUser->id}}">
                                            {{ $sUser->name }}
                                        </option>
                                    @endforeach
                                </select>
                                
                                @error('support_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="user_id" class="col-md-4 col-form-label text-md-right">
                                Created by: 
                            </label>

                            <div class="col-md-6">
                                <select name="user_id" id="user_id" class="form-control">
                                    @foreach($memberUser as $mUser)
                                        <option value="{{$mUser->id}}">
                                            {{ $mUser->name }}
                                        </option>
                                    @endforeach
                                </select>
                                
                                @error('user_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                        

                        <div class="form-group row">
                            <label for="status" class="col-md-4 col-form-label text-md-right">
                                Requested item:
                            </label>

                            <div class="col-md-6">
                                <select name="item_id" id="item_id" class="form-control">
                                    <option value=""> Select an item </option>
                                    @foreach($items->sortBy('type')->all() as $item)
                                        <option value="{{$item->id}}">
                                            {{ $item->type }} - {{ $item->name }}
                                        </option>
                                    @endforeach
                                </select>
                                
                                @error('item_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
