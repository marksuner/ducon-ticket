<div class="settings">
    <div class="card mb-3">
        <h4 class="card-title mb-0">
            User Credentials
        </h4>

        <div class="card-body">
            <div class="row">
                <div class="col-6">
                    <input type="password" class="form-control" placeholder="Password">
                </div>
                <div class="col-6">
                    <input type="password" class="form-control" placeholder="Confirm Password">
                </div>
            </div>
        </div>
    </div>

    <div class="card mb-3">
        <h4 class="card-title mb-0">
            User Info
        </h4>

        <div class="card-body">
            <div class="row mb-2">
                <div class="col-5">
                    <label for="">Last Name</label>
                    <input type="text" class="form-control" placeholder="">
                </div>
                <div class="col-5">
                    <label for="">First Name</label>
                    <input type="text" class="form-control" placeholder="">
                </div>
                <div class="col-2">
                    <label for="">Middle Name</label>
                    <input type="text" class="form-control" placeholder="">
                </div>
            </div>
            <div class="row">
                <div class="col-7">
                    <label for="">Address</label>
                    <textarea name="" class="form-control" cols="30" rows="10"></textarea>
                </div>
                <div class="col-5">
                    <div class="row">
                        <div class="col-12">
                            <label for="">Company ID</label>
                            <input type="text" class="form-control" placeholder="">
                        </div>
                        <div class="col-12">
                            <label for="">Mobile Number</label>
                            <input type="text" class="form-control" placeholder="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mb-5">
        <button class="btn btn-primary float-right">
            Update
        </button>
    </div>
</div>