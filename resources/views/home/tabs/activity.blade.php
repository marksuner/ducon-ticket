@for($i = 0; $i <= 10; $i++)

    <div class="activity">
        <div class="row">
            <div class="col-1 activity--avatar-holder">
                <img src="//placehold.it/150" width="100%">
            </div>
            <div class="col-10 no-gutters">
                <div class="text-primary"> Ticket # - Title </div>
                <div class="text-muted activity--time"> Created last: January 2020 </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 text-muted activity--description">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium, fugiat maxime. Iusto iure harum voluptate laudantium? Temporibus expedita cum dicta vitae voluptatibus quisquam, voluptatem itaque, ullam molestiae iste delectus repellat.
            </div>
        </div>
        <div class="row activity--actionable">
            <div class="col-12">
                <ul class="list-inline">
                    <li class="list-inline-item">
                        <i class="fas fa-share"></i> Share
                    </li>
                    <li class="list-inline-item">
                        <i class="fas fa-comment"></i> Comment (5)
                    </li>
                </ul>
            </div>
        </div>
        <div class="row activity--form">
            <textarea name="comment" class="form-control" placeholder="Type a comment" rows="1"></textarea>
        </div>
    </div>

@endfor