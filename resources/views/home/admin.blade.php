<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-3 m-1 alert alert-primary">
                <strong>Top Support</strong>
                <i style="opacity: 0.4" class="fas fa-ticket-alt fa-2x float-right"></i>
            </div>
            <div class="col-md-3 m-1 alert alert-success">
                <strong>Most Requested</strong>
                <i style="opacity: 0.4" class="fas fa-reply fa-2x float-right"></i>
            </div>
            <div class="col-md-3 m-1 alert alert-success">
                <strong>Audit Trails</strong>
                <i style="opacity: 0.4" class="fas fa-pencil-alt fa-2x float-right"></i>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <div id="dashboard-calendar"></div>
    </div>
</div>