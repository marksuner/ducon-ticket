<div class="card">
    <div class="card-body">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs">
            <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#activity">Activity</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#timeline">Timeline</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#settings">Settings</a>
            </li>
        </ul>
        
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane active container" id="activity">
                @include('home.tabs.activity')
            </div>
            <div class="tab-pane container" id="timeline">
                @include('home.tabs.timeline')
            </div>
            <div class="tab-pane container" id="settings">
                @include('home.tabs.settings')
            </div>
        </div>
    </div>
</div>