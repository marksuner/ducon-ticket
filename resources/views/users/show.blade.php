@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="row">
                <div class="col-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">
                                Audit Trail
                            </h4>
                            <div class="users-show--list-container">
                                @foreach($user->trails as $trail)
                                    <div class="notification">
                                        <span class="float-right notification-date">
                                            {{ $trail->created_at->diffForHumans() }}
                                        </span>
                                        <div class="notification-body" data-toggle="tooltip" title="{{$trail->description}}">
                                            {{ $trail->description }}
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">
                                Notifications tray
                            </h4>
                            <div class="users-show--list-container">
                                @foreach($user->notifications as $notification)
                                    <div class="notification">
                                        <span class="float-right notification-date">
                                            {{ $notification->created_at->diffForHumans() }}
                                        </span>

                                        <div class="notification-title" data-toggle="tooltip" title="{{$notification->title}}">
                                            {{ $notification->title }}
                                        </div>
                                        <div class="notification-body" data-toggle="tooltip" title="{{$notification->content}}">
                                            {{ $notification->content }}
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">
                        Statistics
                    </h4>
                    <div id="users-show--canvas-container">
                        <canvas id="users-show--canvas"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
