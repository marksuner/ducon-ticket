@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Users</div>

                <div class="card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th colspan="20">

                                    <form action="/users" method="get">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group mb-2">
                                                    <label for="name" class="sr-only">Name</label>
                                                    <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group mb-2">
                                                    <label for="email" class="sr-only">Email</label>
                                                    <input type="text" class="form-control" id="email" name="email" placeholder="Email">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group mb-2">
                                                    <label for="role" class="sr-only">Role</label>
                                                    <select name="role_id" class="form-control">
                                                        <option value="">Select a role</option>
                                                        @foreach($roles as $role)
                                                            <option value="{{$role->id}}">
                                                                {{ $role->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <button type="submit" class="btn btn-primary mb-2">
                                                    Search
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Email</th>
                                <th>Name</th>
                                <th>Role</th>
                                <th>
                                    <a href="/users/create" class="btn btn-sm btn-primary">
                                        Create
                                    </a>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td> <a href="/users/{{$user->id}}">{{ id_pad($user->id) }} </a></td>
                                    <td> {{ $user->email }} </td>
                                    <td> {{ $user->name }} </td>
                                    <td> {{ $user->role->name }} </td>
                                    <td>
                                        <a href="/users/{{$user->id}}/edit" class="btn btn-sm btn-secondary">
                                            <i class="far fa-edit"></i>
                                        </a>
                                        <a href="#" class="btn btn-sm btn-danger delete" data-url="/users/{{$user->id}}">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="10" class="align-content-center">
                                    {{ $users->links() }}
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
