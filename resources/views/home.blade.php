@extends('layouts.app')

@section('content')
<div class="container">
    @if(is_admin())
        @include('home.admin')        
    @else
        @include('home.non-admin')
    @endif
</div>
@endsection
