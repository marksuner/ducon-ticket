import { Calendar } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';

document.addEventListener('DOMContentLoaded', function() {
  let calendarEl = document.getElementById('dashboard-calendar');

  let calendar = new Calendar(calendarEl, {
    header: { 
      left:   'prev,next today',
      center: 'title',
      right:  'dayGridMonth,timeGridWeek'
    },
    plugins: [ dayGridPlugin, timeGridPlugin, listPlugin ]
  });

  calendar.render();
});