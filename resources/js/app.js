/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
const toastr = require('toastr');

// window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// const app = new Vue({
//     el: '#app',
// });

/**
 * Global Functions
 */
$('.delete').on('click', (e) => {
    const url = $(e.target).data('url');

    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.value) {
            return axios.delete(url)
        }
        return false;
    }).then((result) => {
        if (result) {
            Swal.fire(
                'Deleted!',
                'Resource has been deleted.',
                'success'
            )
        }
    }).then(() => {
        setTimeout(() => {
            window.location.reload();
        }, 1500);
    })
});

$('[data-toggle="tooltip"]').tooltip()

// const toast = {
//     success: $('#toast-success'),
// };

// if(toast.success) {
//     toastr.success(toast.success.data('message'))
// }


/**
 * users.show
 */
require('./users-show');

/**
 * dashboard
 */
require('./dashboard');