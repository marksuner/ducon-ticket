<?php

namespace App;

use App\Role;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $dispatchesEvents = [
        'saved' => \App\Events\ModelSaved::class,
        'created' => \App\Events\ModelCreated::class,
        'updated' => \App\Events\ModelUpdated::class,
        'deleted' => \App\Events\ModelDeleted::class,
    ];

    public function person() {
        return $this->hasOne('App\Person');
    }

    public function role(){
        return $this->belongsTo('App\Role');
    }

    public function notifications() {
        return $this->hasMany('App\Notification');
    }

    public function trails() {
        return $this->hasMany('App\Trail');
    }

    private function getByRole($query, $name)
    {
        $role = Role::where('name', $name)->first();

        return $query->where('role_id', $role->id);
    }

    public function scopeAdmin($query)
    {
        return $this->getByRole($query, 'admin');
    }

    public function scopeSupport($query)
    {
        return $this->getByRole($query, 'support');
    }

    public function scopeMember($query)
    {
        return $this->getByRole($query, 'member');
    }

}
