<?php

namespace App\Providers;

use View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('layouts.app', function ($view) {
            $notifications = [];

            if (!auth()->guest()) {
                $notifications = auth()
                    ->user()
                    ->notifications()
                    ->orderBy('created_at', 'desc')
                    ->limit(20)
                    ->get();
            }

            $view->with('notifications', $notifications);
        });
    }
}
