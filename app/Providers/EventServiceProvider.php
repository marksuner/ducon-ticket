<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        \App\Events\ModelSaved::class => [
            \App\Listeners\ModelSaved::class,
        ],
        \App\Events\ModelCreated::class => [
            \App\Listeners\ModelCreated::class,
        ],
        \App\Events\ModelUpdated::class => [
            \App\Listeners\ModelUpdated::class,
        ],
        \App\Events\ModelDeleted::class => [
            \App\Listeners\ModelDeleted::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
