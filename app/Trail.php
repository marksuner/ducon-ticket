<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trail extends Model
{
    public $fillable = [
        'user_id',
        'description',
        'old_aux',
        'new_aux',
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }
}
