<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = [
        'user_id',
        'admin_id',
        'supporter_id',
        'item_id',
        'status',
        'title',
        'comment',
        'seen_at',
    ];

    protected $dispatchesEvents = [
        'saved' => \App\Events\ModelSaved::class,
        'created' => \App\Events\ModelCreated::class,
        'updated' => \App\Events\ModelUpdated::class,
        'deleted' => \App\Events\ModelDeleted::class,
    ];

    public function createdBy() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function administrativeBy() {
        return $this->belongsTo('App\User', 'admin_id');
    }

    public function supportedBy() {
        return $this->belongsTo('App\User', 'supported_id');
    }

    public function item() {
        return $this->belongsTo('App\Item');
    }
}
