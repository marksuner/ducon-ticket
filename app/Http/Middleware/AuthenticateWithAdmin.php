<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthenticateWithAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guest()) {
            return redirect('/login');
        }

        $role = Auth::user()->role->name;

        if ($role !== 'admin') {
            Auth::logout();
            return redirect('/login');
        }

        return $next($request);
    }
}
