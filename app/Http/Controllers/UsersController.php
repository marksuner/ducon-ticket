<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Requests\UserCreateRequest;

class UsersController extends Controller
{
    private $users;
    private $roles;

    public function __construct(User $users, Role $roles)
    {
        $this->users = $users;
        $this->roles = $roles;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = $request->all();
        $likes = ['name', 'email'];

        $users = $this->searchQuery($this->users, $data, $likes);
        $users = $users->with('role')->paginate(20);
        $roles = $this->roles->get();

        return view('users.index', compact('users', 'roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $roles = $this->roles->get();
        return view('users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\UserCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserCreateRequest $request)
    {
        $user = $this->users->create([
            'name' => $request->input('first_name') . ' ' . $request->input('last_name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'role_id' => $request->input('role_id'),
        ]);

        $user->person()->create([
            'last_name' => $request->input('last_name'),
            'first_name' => $request->input('first_name'),
            'mobile_no' => $request->input('mobile_no'),
        ]);

        session('success', 'Successfully created user');
        
        return redirect('/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = $this->users->with(['person', 'trails', 'notifications', 'role'])->find($id);

        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->users->with(['person', 'role'])->find($id);
        $roles = $this->roles->get();
        
        return view('users.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = $this->users->find($id);

        $userInput = [
            'name' => $request->input('first_name') . ' ' . $request->input('last_name'),
            'email' => $request->input('email'),
            'role_id' => $request->input('role_id'),
        ];

        if($request->has('password')) {
            $userInput['password'] = bcrypt($request->input('password'));
        }

        $user->update($userInput);

        $user->person()->update([
            'last_name' => $request->input('last_name'),
            'first_name' => $request->input('first_name'),
            'mobile_no' => $request->input('mobile_no'),
        ]);

        session('success', 'Successfully updated user');
        
        return redirect('/users/' . $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->users->destroy($id);

        if(request()->wantsJson()) {
            return response([], 204);
        }

        session('success', 'Successfully deleted user');
        
        return redirect('/users');
    }
}
