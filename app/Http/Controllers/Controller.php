<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function searchQuery($model, $data = [], $likes = []) {
        
        foreach($data as $key => $value) {
            if (empty($value)) {
                unset($data[$key]);
            }
        }

        foreach($likes as $like) {
            if (array_key_exists($like, $data)) {
                $data[$like] = '%' . $data[$like] . '%';
            }
        }

        foreach($data as $key => $value) {
            $model = in_array($key, $likes)
                ? $model->where($key, 'like', $value)
                : $model->where($key, $value);
        }

        return $model;
    }
}
