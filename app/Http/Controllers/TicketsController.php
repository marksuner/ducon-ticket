<?php

namespace App\Http\Controllers;

use App\User;
use App\Ticket;
use App\Item;
use Illuminate\Http\Request;
use App\Http\Requests\TicketCreateRequest;

class TicketsController extends Controller
{
    private $tickets;
    private $users;
    private $items;

    public function __construct(Ticket $tickets, User $users, Item $items)
    {
        $this->tickets = $tickets;
        $this->users = $users;
        $this->items = $items;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = $request->all();
        $likes = ['title', 'comment'];
        $tickets = $this->searchQuery($this->tickets, $data, $likes);
        $tickets = $tickets->with(['createdBy', 'administrativeBy', 'supportedBy', 'item'])->paginate(20);

        $adminUser = $this->users->admin()->get();
        $supportUser = $this->users->support()->get();
        $memberUser = $this->users->member()->get();

        return view('tickets.index', compact('tickets', 'supportUser', 'memberUser', 'adminUser'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $supportUser = $this->users->support()->get();
        $memberUser = $this->users->member()->get();
        $items = $this->items->get();

        return view('tickets.create', compact('supportUser', 'memberUser', 'items'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TicketCreateRequest $request)
    {
        $ticket = $this->tickets->create($request->all());

        session('success', 'Successfully created a ticket');

        return redirect('/tickets');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $supportUser = $this->users->support()->get();
        $memberUser = $this->users->member()->get();
        $items = $this->items->get();
        $ticket = $this->tickets->find($id);

        return view('tickets.edit', compact('ticket', 'supportUser', 'memberUser', 'items'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ticket = $this->tickets->find($id)->update($request->all());

        session('success', 'Successfully updated a ticket');

        return redirect('/tickets');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->tickets->destroy($id);

        if(request()->wantsJson()) {
            return response([], 204);
        }

        session('success', 'Successfully deleted ticket');
        
        return redirect('/tickets');
    }
}
