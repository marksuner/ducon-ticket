<?php

namespace App\Events;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Queue\SerializesModels;

class ModelCreated
{
    use SerializesModels;

    public $model;

    /**
     * Create a new event instance.
     *
     * @param \App\Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }
    
}
