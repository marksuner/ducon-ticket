<?php

namespace App\Listeners;

use App\Trail;
use App\Events\ModelDeleted as ModelDeletedEvent;

class ModelDeleted
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ModelDeletedEvent $event)
    {
        $cd = explode('\\', get_class($event->model));
        $name = $cd[count($cd) - 1];
        
        Trail::create([
            'user_id' => \Auth::user()->id,
            'description' => 'deleted a ' . $name,
            'new_aux' => $event->model->toJson(),
        ]);
    }
}
