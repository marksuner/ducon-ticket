<?php

namespace App\Listeners;

use App\Trail;
use App\Events\ModelUpdated as ModelUpdatedEvent;

class ModelUpdated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ModelUpdatedEvent $event)
    {
        $cd = explode('\\', get_class($event->model));
        $name = $cd[count($cd) - 1];
        
        Trail::create([
            'user_id' => \Auth::user()->id,
            'description' => 'updated a ' . $name,
            'new_aux' => $event->model->toJson(),
        ]);
    }
}
