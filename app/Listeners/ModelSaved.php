<?php

namespace App\Listeners;

use App\Events\ModelSaved as ModelSavedEvent;

class ModelSaved
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ModelSavedEvent $event)
    {
        // app('log')->info($event->model);
    }
}
