<?php
use Auth;
use App\Role;
use Illuminate\Support\Str;

if (!function_exists('id_pad')) {
    function id_pad($value) {
        return str_pad($value, 5,'0', STR_PAD_LEFT);
    }
}


if (!function_exists('route_active')) {
    function route_active($value) {
        return (request()->is($value)) 
            ? 'active'
            : '';
    }
}

if (!function_exists('app_str_limit')) {
    function app_str_limit($value, $limit = 50) {
        return Str::limit($value, $limit, '...');
    }
}

if (!function_exists('is_admin')) {
    function is_admin() {
        $role = Role::where('name', 'admin')->first();
        return Auth::user()->role_id === $role->id;
    }
}
